package di.unimi.lucacervello.geopost;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Locale;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import di.unimi.lucacervello.geopost.client.model.Profile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateStatusActivity extends AppCompatActivity {
    public static final String TAG = "UpdateStatusActivity";

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private static final GeoPostService mGeoPostService = GeoPostClient.getService();
    private Profile mProfile;
    private String mSessionId;

    private Toolbar mToolbar;
    private TextView mTextViewLong;
    private TextView mTextViewLat;
    private EditText mTextViewMessage;

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;

    @Override
    protected void onCreate(Bundle savedIntanceState) {
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_update_status);

        mSessionId = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                .getString(getString(R.string.session_id), null);

        if (mSessionId == null) {
            startActivity(new Intent(UpdateStatusActivity.this, LoginActivity.class));
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mToolbar = findViewById(R.id.app_toolbar);
        mTextViewLong = findViewById(R.id.longitude_text);
        mTextViewLat = findViewById(R.id.latitude_text);
        mTextViewMessage = findViewById(R.id.profile_message);

        mGeoPostService.getProfile(mSessionId).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                mProfile = response.body();
                mToolbar.setTitle(mProfile.getUsername());
                mTextViewLat.setText(String.format(Locale.ITALIAN, "%4.2f", mProfile.getLat()));
                mTextViewLong.setText(String.format(Locale.ITALIAN, "%4.2f", mProfile.getLon()));
                mTextViewMessage.setText(mProfile.getMsg());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }

    public void saveStatus(View view) {

        mGeoPostService.updateStatus(mSessionId,
                mTextViewMessage.getText().toString(),
                mTextViewLat.getText().toString().replace(",", "."),
                mTextViewLong.getText().toString().replace(",", "."))
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        startActivity(new Intent(UpdateStatusActivity.this, FollowedActivity.class));
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {

                    }
                });
    }

    private void checkGps() {

        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(500);
        locationRequest.setInterval(500);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        mSettingsClient.checkLocationSettings(builder.build())
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ResolvableApiException) {
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                resolvable.startResolutionForResult(UpdateStatusActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sendEx) {
                                sendEx.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void getLongLan(View view) {
        checkGps();
        checkPermissions();
        getLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            Toast.makeText(getApplicationContext(), "Presa location", Toast.LENGTH_LONG).show();
                            mTextViewLat.setText(String.format(Locale.ITALIAN, "%4.2f", location.getLatitude()));
                            mTextViewLong.setText(String.format(Locale.ITALIAN, "%4.2f", location.getLongitude()));
                        } else {
                            Log.i(TAG, "Location null");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User aggred location");
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User not aggred location");
                        break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("HELLOOOOOO granted");
                    getLocation();
                }
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
