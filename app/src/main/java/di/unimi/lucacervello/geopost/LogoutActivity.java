package di.unimi.lucacervello.geopost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutActivity extends AppCompatActivity {
    public static final String TAG = "LogoutActivity";
    private static final GeoPostService mGeoPostService = GeoPostClient.getService();
    private String mSessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        mSessionId = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                .getString(getString(R.string.session_id), null);
    }

    public void logout(View view) {

        mGeoPostService.revokeSessionId(mSessionId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                        .edit()
                        .remove(getString(R.string.session_id))
                        .apply();
                startActivity(new Intent(LogoutActivity.this, LoginActivity.class));
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}
