package di.unimi.lucacervello.geopost.client.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class Followeds {

    @SerializedName("followed")
    @Expose
    private List<Profile> followed = null;

    public List<Profile> getFollowed() {
        return followed;
    }

    public void setFollowed(List<Profile> followed) {
        this.followed = followed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Followeds)) return false;
        Followeds followeds = (Followeds) o;
        return Objects.equals(getFollowed(), followeds.getFollowed());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getFollowed());
    }

    @Override
    public String toString() {
        return "Followeds{" +
                "followed=" + followed +
                '}';
    }
}
