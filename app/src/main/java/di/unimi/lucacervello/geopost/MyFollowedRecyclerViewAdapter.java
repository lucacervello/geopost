package di.unimi.lucacervello.geopost;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import di.unimi.lucacervello.geopost.FollowedFragment.OnListFragmentInteractionListener;
import di.unimi.lucacervello.geopost.client.model.Profile;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Profile} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyFollowedRecyclerViewAdapter extends RecyclerView.Adapter<MyFollowedRecyclerViewAdapter.ViewHolder> {

    private final List<Profile> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyFollowedRecyclerViewAdapter(List<Profile> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_followed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getUsername());
        holder.mMessageView.setText(mValues.get(position).getMsg());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mMessageView;
        public final TextView mContentView;
        public Profile mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMessageView = view.findViewById(R.id.profile_message);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
