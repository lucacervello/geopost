package di.unimi.lucacervello.geopost.client.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Usernames {

    @SerializedName("usernames")
    @Expose
    private List<String> usernames = null;

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }
}