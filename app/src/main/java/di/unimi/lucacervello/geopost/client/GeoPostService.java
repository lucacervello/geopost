package di.unimi.lucacervello.geopost.client;

import di.unimi.lucacervello.geopost.client.model.Followeds;
import di.unimi.lucacervello.geopost.client.model.Profile;
import di.unimi.lucacervello.geopost.client.model.Usernames;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GeoPostService {

    @FormUrlEncoded
    @POST("login")
    Call<String> getSessionId(@Field("username") String username,
                              @Field("password") String password);

    @GET("followed")
    Call<Followeds> getFollowed(@Query("session_id") String sessionId);

    @GET("status_update")
    Call<Void> updateStatus(@Query("session_id") String sessionId,
                            @Query("message") String message,
                            @Query("lat") String latitude,
                            @Query("lon") String longitude);

    @GET("logout")
    Call<Void> revokeSessionId(@Query("session_id") String sessionId);

    @GET("users")
    Call<Usernames> getUsernames(@Query("session_id") String sessionId,
                                 @Query("usernamestart") String usernameStart,
                                 @Query("limit") Integer limit);

    @GET("follow")
    Call<String> follow(@Query("session_id") String sessionId,
                        @Query("username") String username);

    @GET("profile")
    Call<Profile> getProfile(@Query("session_id") String sessionId);

}
