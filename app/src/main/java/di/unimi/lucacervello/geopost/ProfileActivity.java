package di.unimi.lucacervello.geopost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import di.unimi.lucacervello.geopost.client.model.Profile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = "ProfileActivity";
    private static final GeoPostService mGeoPostService = GeoPostClient.getService();
    private String mSessionId;

    private Toolbar mToolbar;
    private TextView mTextViewLong;
    private TextView mTextViewLat;
    private TextView mTextViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Profile profile = getIntent().getParcelableExtra("profile");

        Boolean isFollowed = getIntent().getBooleanExtra("isFollowed", true);

        if (isFollowed) {
            findViewById(R.id.fab).setVisibility(View.INVISIBLE);
        }

        mSessionId = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                .getString(getString(R.string.session_id), null);

        if (mSessionId == null) {
            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
        }

        Log.i(TAG, "Profile : " + profile);
        mToolbar = findViewById(R.id.search_toolbar);
        mToolbar.setTitle(profile.getUsername());
        mTextViewLong = findViewById(R.id.longitude_text);
        mTextViewLong.setText(String.format(Locale.ITALIAN, "%4.2f", profile.getLon()));
        mTextViewLat = findViewById(R.id.latitude_text);
        mTextViewLat.setText(String.format(Locale.ITALIAN, "%4.2f", profile.getLat()));
        mTextViewMessage = findViewById(R.id.profile_message);
        mTextViewMessage.setText(profile.getMsg());
    }

    public void logout(View view) {

        mGeoPostService.revokeSessionId(mSessionId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
