package di.unimi.lucacervello.geopost;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences =
                getSharedPreferences(getString(R.string.session_preference),
                        Context.MODE_PRIVATE);

        String sessionId = sharedPreferences.getString(getString(R.string.session_id), null);

        if (sessionId == null) {
            Log.i(TAG, "Session id not present, go to login");
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            Log.i(TAG, "Session id present, got to followed");
            startActivity(new Intent(this, FollowedActivity.class));
        }
    }
}
