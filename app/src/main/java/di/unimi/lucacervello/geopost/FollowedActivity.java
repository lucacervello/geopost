package di.unimi.lucacervello.geopost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import di.unimi.lucacervello.geopost.client.model.Followeds;
import di.unimi.lucacervello.geopost.client.model.Profile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowedActivity extends AppCompatActivity implements FollowedFragment.OnListFragmentInteractionListener, OnMapReadyCallback {
    public static final String TAG = "FollowedActivity";

    private final static GeoPostService mGeoPostService = GeoPostClient.getService();
    private List<Profile> mFolloweds = new ArrayList<>();
    private String mSessionId;
    private GoogleMap mGoogleMap;
    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_followed);


        mSessionId = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                .getString(getString(R.string.session_id), null);

        if (mSessionId == null) {
            startActivity(new Intent(FollowedActivity.this, LoginActivity.class));
        }

        final FollowedFragment followedFragment = (FollowedFragment) getSupportFragmentManager().findFragmentById(R.id.headlines_fragment);

        mGeoPostService.getFollowed(mSessionId).enqueue(new Callback<Followeds>() {
            @Override
            public void onResponse(Call<Followeds> call, Response<Followeds> response) {
                mFolloweds.addAll(response.body().getFollowed());
                followedFragment.updateFolloweds(mFolloweds);
                onMapReady(mGoogleMap);
            }

            @Override
            public void onFailure(Call<Followeds> call, Throwable t) {
                t.printStackTrace();
            }
        });

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        final MyGestureListener myGestureListener = new MyGestureListener();
        mDetector = new GestureDetectorCompat(this, myGestureListener);
        findViewById(R.id.headlines_fragment).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
    }


    @Override
    public void onListFragmentInteraction(Profile item) {
        Intent intent = new Intent(FollowedActivity.this, ProfileActivity.class);
        intent.putExtra("profile", item);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FollowedActivity.this, LogoutActivity.class));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        for (Profile profile : mFolloweds) {
            Log.i(TAG, "profile : " + profile);
            mGoogleMap.addMarker(new MarkerOptions().position(
                    new LatLng(profile.getLat(), profile.getLon()))
                    .title(profile.getUsername())
                    .snippet(profile.getMsg()));
        }
    }

    @Override
    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    public void updateStatus(View view) {
        startActivity(new Intent(FollowedActivity.this, UpdateStatusActivity.class));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener implements GestureDetector.OnDoubleTapListener {
        public static final String DEBUG_TAG = "MyGestureListener";


        @Override
        public boolean onDoubleTap(MotionEvent e) {
            onSearchRequested();
            System.out.println(DEBUG_TAG + " tapEvent");
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            onSearchRequested();
            System.out.println(DEBUG_TAG + " tapEvent");
            return true;
        }
    }
}
