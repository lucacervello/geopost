package di.unimi.lucacervello.geopost;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";

    private EditText editTextUsername;
    private EditText editTestPassword;
    private String sessionId;
    private SharedPreferences sharedPreferences;
    private GeoPostService geoPostService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        geoPostService = GeoPostClient.getService();
        editTextUsername = findViewById(R.id.username);
        editTestPassword = findViewById(R.id.password);
        sharedPreferences = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE);
        sessionId = sharedPreferences.getString(getString(R.string.session_id), null);
    }

    public void login(View view) {
        Log.i(TAG, "Try login");
        Log.i(TAG, "Session id : " + sessionId);
        if (sessionId == null) {
            Log.i(TAG, "Session id null, external call");

            geoPostService.getSessionId(editTextUsername.getText().toString(), editTestPassword.getText().toString())
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            sessionId = response.body();
                            updateSessionId(sessionId);
                            startActivity(new Intent(LoginActivity.this, FollowedActivity.class));
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            t.printStackTrace();
                            sessionId = null;
                        }
                    });
        } else {
            startActivity(new Intent(LoginActivity.this, FollowedActivity.class));
        }
    }

    private void updateSessionId(String sessionId) {
        sharedPreferences.edit().putString(getString(R.string.session_id), sessionId).apply();
    }
}
