package di.unimi.lucacervello.geopost;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import di.unimi.lucacervello.geopost.client.GeoPostClient;
import di.unimi.lucacervello.geopost.client.GeoPostService;
import di.unimi.lucacervello.geopost.client.model.Usernames;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchableActivity extends AppCompatActivity implements SearchableAdapter.OnListInteractionListener {

    private GeoPostService mGeoPostService = GeoPostClient.getService();
    private String mSessionId;
    private List<String> mUsernames = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_recycler_view);

        mRecyclerView = findViewById(R.id.search_recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new SearchableAdapter(mUsernames, this);
        mRecyclerView.setAdapter(mAdapter);


        mSessionId = getSharedPreferences(getString(R.string.session_preference), Context.MODE_PRIVATE)
                .getString(getString(R.string.session_id), null);

        if (mSessionId == null) {
            startActivity(new Intent(SearchableActivity.this, LoginActivity.class));
        }

        Intent intent = getIntent();

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            mGeoPostService.getUsernames(mSessionId, query, 5).enqueue(new Callback<Usernames>() {
                @Override
                public void onResponse(Call<Usernames> call, Response<Usernames> response) {
                    mUsernames.addAll(response.body().getUsernames());
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<Usernames> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error : ", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onListInteraction(String username) {

        mGeoPostService.follow(mSessionId, username).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                startActivity(new Intent(SearchableActivity.this, FollowedActivity.class));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}
