package di.unimi.lucacervello.geopost.client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class GeoPostClient {

    public static GeoPostService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://ewserver.di.unimi.it/mobicomp/geopost/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(GeoPostService.class);
    }

}
