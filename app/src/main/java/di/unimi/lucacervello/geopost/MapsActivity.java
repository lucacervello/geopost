package di.unimi.lucacervello.geopost;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public class MapsActivity extends FragmentActivity {

    private static final String TAG = "MapsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
    }

    public void list(View view) {
        startActivity(new Intent(MapsActivity.this, FollowedActivity.class));
    }
}
