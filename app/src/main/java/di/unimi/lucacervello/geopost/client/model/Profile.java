package di.unimi.lucacervello.geopost.client.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

@Entity
public class Profile implements Parcelable {

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
    @PrimaryKey
    @NonNull
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;

    private Profile(Parcel in) {
        setUsername(in.readString());
        setMsg(in.readString());
        setLat(in.readDouble());
        setLon(in.readDouble());
    }

    public Profile() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Profile username(String username) {
        setUsername(username);
        return this;
    }

    public Profile msg(String msg) {
        setMsg(msg);
        return this;
    }

    public Profile lat(Double lat) {
        setLat(lat);
        return this;
    }

    public Profile lon(Double lon) {
        setLon(lon);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Profile)) return false;
        Profile profile = (Profile) o;
        return Objects.equals(getUsername(), profile.getUsername()) &&
                Objects.equals(getMsg(), profile.getMsg()) &&
                Objects.equals(getLat(), profile.getLat()) &&
                Objects.equals(getLon(), profile.getLon());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getUsername(), getMsg(), getLat(), getLon());
    }

    @Override
    public String toString() {
        return "Profile{" +
                "username='" + username + '\'' +
                ", msg='" + msg + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUsername());
        parcel.writeString(getMsg());
        parcel.writeDouble(getLat());
        parcel.writeDouble(getLon());
    }
}