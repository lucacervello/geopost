package di.unimi.lucacervello.geopost;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SearchableAdapter extends RecyclerView.Adapter<SearchableAdapter.ViewHolder> {

    private List<String> mUsernames;
    private OnListInteractionListener mListener;

    public SearchableAdapter(List<String> usernames, OnListInteractionListener listener) {
        mUsernames = usernames;
        mListener = listener;
    }

    @Override
    public SearchableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.searchable_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mUsernames.get(position);
        holder.mSearchableViewText.setText(mUsernames.get(position));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mListener) {
                    mListener.onListInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.mUsernames.size();
    }

    public interface OnListInteractionListener {
        void onListInteraction(String username);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mSearchableViewText;
        public View mView;
        public String mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSearchableViewText = view.findViewById(R.id.searchable_text);
        }
    }

}
