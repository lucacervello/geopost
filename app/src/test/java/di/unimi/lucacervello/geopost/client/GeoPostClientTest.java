package di.unimi.lucacervello.geopost.client;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import di.unimi.lucacervello.geopost.client.model.Followeds;
import di.unimi.lucacervello.geopost.client.model.Profile;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GeoPostClientTest {

    private final GeoPostService geoPostService =
            GeoPostClient.getService();

    private String sessionId;

    @Before
    public void setup() throws IOException {
        this.sessionId = geoPostService
                .getSessionId("lctest1", "lctest1")
                .execute()
                .body();
    }

    @After
    public void tearDown() throws IOException {
        geoPostService.revokeSessionId(sessionId)
                .execute();
    }

    @Test
    @Ignore
    public void testGetFollowed() throws IOException {
        Response<Followeds> response = geoPostService
                .getFollowed(sessionId)
                .execute();

        Followeds followeds = new Followeds();
        followeds.setFollowed(Arrays.asList(new Profile().username("lucacervellogeopost"),
                new Profile().username("lucacervellogeopost1")));

        assertEquals(response.code(), 200);
        assertEquals(followeds, response.body());
        assertTrue(followeds.equals(response.body()));
    }

    @Test
    public void testGetProfile() throws IOException {
        Response<Profile> response = geoPostService
                .getProfile(sessionId)
                .execute();

        assertEquals(response.code(), 200);
        Profile profile = new Profile()
                .username("lctest1")
                .msg("ciao hello")
                .lat(15.7)
                .lon(12.4);
        assertEquals(profile, response.body());
    }

    @Test
    public void testSetProfile() throws IOException {
        Response<Void> response = geoPostService.updateStatus(sessionId, "Test", "15.7", "12.4")
                .execute();

        assertEquals(response.code(), 200);
    }


}